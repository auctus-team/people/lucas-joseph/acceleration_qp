#include "acceleration_qp/controller.h"

namespace QPController
{
bool Acceleration::init(std::string robot_description, std::vector<std::string> joint_names) 
{
    //--------------------------------------
    // LOAD ROBOT
    //--------------------------------------
    if (!load_robot_model(robot_description,joint_names))
        return false;
    if (!control_frame_set)
        controlled_frame = model.frames[model.nframes-1].name;
    setRegularizationWeight(1e-5);
    setControlPeriod(0.001);
    kp_reg = Eigen::MatrixXd::Ones(7,1) * 1.0; 
    q_mean = (model.upperPositionLimit + model.lowerPositionLimit) / 2.0 ;

    //--------------------------------------
    // QPOASES
    //--------------------------------------
    number_of_variables = model.nv;
    // number_of_constraints = 3 * model.nv;
    number_of_constraints = 0;
    q3dot_constraint.resize(model.nv);
    q2dot_constraint.resize(model.nv);
    qdot_constraint.resize(model.nv);
    q_constraint.resize(model.nv);
    // qp_solution.resize(model.nv);
    qp = qp_solver.configureQP(number_of_variables, number_of_constraints);
    return true;
}

Eigen::VectorXd Acceleration::update(Eigen::VectorXd q, Eigen::VectorXd qdot, Eigen::VectorXd qdot_d, Eigen::VectorXd q2dot_d, Eigen::Matrix<double,6,1> xdd_star)
{
    // Update the model
    // First calls the forwardKinematics on the model, then computes the placement of each frame.
    pinocchio::forwardKinematics(model,data,q,qdot,0.0*qdot);
    pinocchio::updateFramePlacements(model,data);
    pinocchio::computeJointJacobians(model,data,q);
    pinocchio::getFrameJacobian(model, data, model.getFrameId(controlled_frame), pinocchio::ReferenceFrame::WORLD, J);
    Jdot_qdot = pinocchio::getFrameClassicalAcceleration(model, data, model.getFrameId(controlled_frame),pinocchio::ReferenceFrame::WORLD).toVector();

    // Formulate QP problem such that
    // joint_torque_out = argmin 1/2 tau^T H tau + tau^T g_
    //                       s.t     lbA < A tau < ubA
    //                                   lb < tau < ub 
    
      // Regularisation task
    qp.hessian =  2.0 * regularisation_weight * Eigen::MatrixXd::Identity(model.nv,model.nv);
    qp.gradient = -2.0 * regularisation_weight * (kp_reg.cwiseProduct((q_mean- q)) - 2.0* (kp_reg.cwiseSqrt()).cwiseProduct(qdot) );
    
    a.noalias() = J ;
    b.noalias() =  Jdot_qdot - xdd_star;

    qp.hessian  +=  2.0 * a.transpose() *  a;
    qp.gradient +=  2.0 * a.transpose() * b;    

    double horizon_dt = 1.0 * control_period;

    qp.ub = ( q3dot_max * horizon_dt + q2dot_d ).cwiseMin
            ( q2dot_max ).cwiseMin
            ( (model.velocityLimit - qdot_d)/horizon_dt ).cwiseMin
            ( 2.0 * ( model.upperPositionLimit - q - qdot_d * horizon_dt ) / ( horizon_dt * horizon_dt) );

    qp.lb = (-q3dot_max * horizon_dt + q2dot_d ).cwiseMax
            (-q2dot_max ).cwiseMax
            ( (-model.velocityLimit - qdot_d)/horizon_dt ).cwiseMax
            ( 2.0 * ( model.lowerPositionLimit - q - qdot_d * horizon_dt ) / (horizon_dt * horizon_dt) );
    
    qp_solution = qp_solver.SolveQP(qp);


    return qp_solution;
}

bool Acceleration::load_robot_model(std::string robot_description, std::vector<std::string> joint_names)
{
    // Load the urdf model
    pinocchio::urdf::buildModelFromXML(robot_description,model,false);
    if (!joint_names.empty())
    {
        std::vector<pinocchio::JointIndex> list_of_joints_to_keep_unlocked_by_id;
        for(std::vector<std::string>::const_iterator it = joint_names.begin();
            it != joint_names.end(); ++it)
        {
            const std::string & joint_name = *it;
            if(model.existJointName(joint_name))
            list_of_joints_to_keep_unlocked_by_id.push_back(model.getJointId(joint_name));
            else
            std::cout << "joint: " << joint_name << " does not belong to the model.";
        }
        
        // Transform the list into a list of joints to lock
        std::vector<pinocchio::JointIndex> list_of_joints_to_lock_by_id;
        for(pinocchio::JointIndex joint_id = 1; joint_id < model.joints.size(); ++joint_id)
        {
            const std::string joint_name = model.names[joint_id];
            if(is_in_vector(joint_names,joint_name))
            continue;
            else
            {
            list_of_joints_to_lock_by_id.push_back(joint_id);
            }
        }
        
        // Build the reduced model from the list of lock joints
        Eigen::VectorXd q_rand = randomConfiguration(model);
        model = pinocchio::buildReducedModel(model,list_of_joints_to_lock_by_id,q_rand);
    }
    data = pinocchio::Data(model);

    // Resize and initialise varialbes 
    J.resize(6,model.nv);
    J.setZero();
    nonLinearTerms.resize(model.nv);
    a.resize(6,model.nv);

    return true;
}

pinocchio::Model Acceleration::getRobotModel()
{
    return model;
}

void Acceleration::setControlledFrame(std::string controlled_frame)
{
    this->controlled_frame = controlled_frame; 
    control_frame_set = true;
}

std::string Acceleration::getControlledFrame()
{
    return controlled_frame;
}

void Acceleration::setRegularizationWeight(double regularisation_weight)
{
    this->regularisation_weight = regularisation_weight; 
}

void Acceleration::setq2dotMax(Eigen::VectorXd q2dot_max)
{
    this->q2dot_max = q2dot_max; 
}

void Acceleration::setq3dotMax(Eigen::VectorXd q3dot_max)
{
    this->q3dot_max = q3dot_max; 
}


void Acceleration::setRegularizationGains(Eigen::VectorXd regularisation_gains)
{
    this->kp_reg = regularisation_gains; 
}


void Acceleration::setControlPeriod(double control_period)
{
    this->control_period = control_period; 
}


} // namespace QPController
