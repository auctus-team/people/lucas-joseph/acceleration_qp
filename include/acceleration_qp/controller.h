/*
 * Copyright 2020 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

// Pinocchio
#include "pinocchio/fwd.hpp"
#include "pinocchio/parsers/urdf.hpp"
#include "pinocchio/algorithm/joint-configuration.hpp"
#include "pinocchio/algorithm/kinematics.hpp"
#include "pinocchio/algorithm/jacobian.hpp"
#include "pinocchio/algorithm/aba.hpp"
#include "pinocchio/algorithm/rnea.hpp"
#include "pinocchio/algorithm/crba.hpp"
#include "pinocchio/algorithm/frames.hpp"
#include "pinocchio/multibody/model.hpp"
#include "pinocchio/algorithm/model.hpp"

// Eigen
#include <Eigen/Dense>


// qpOASES
#include <qp_solver/qp_solver.hpp>

namespace QPController
{
class Acceleration
{
public:
    /**
    * \fn bool init
    * \brief Initializes the controller
    * \param ros::NodeHandle& node_handle a ros node handle
    * \param Eigen::VectorXd q_init the robot initial joint position
    * \param Eigen::VectorXd qd_init the robot initial joint velocity
    * \return true if the controller is correctly initialized
    */
    bool init(std::string robot_description, std::vector<std::string> joint_names) ;

    /**
     * \fn Eigen::VectorXd update
     * \brief Update the controller to get the new desired joint torque to send to the robot.
     * \param Eigen::VectorXd q the current joint position of the robot
     * \param Eigen::VectorXd qd the current joint velocity of the robot
     * \param const ros::Duration& period the refresh rate of the control
     * \return A Eigen::VectorXd with the desired joint torque
     */
    Eigen::VectorXd update(Eigen::VectorXd q, Eigen::VectorXd qd, Eigen::VectorXd qdot_d , Eigen::VectorXd q2dot_d,Eigen::Matrix<double,6,1> xd_star);

    template<typename T>
    bool is_in_vector(const std::vector<T> & vector, const T & elt)
    {
      return vector.end() != std::find(vector.begin(),vector.end(),elt);
    }

    void setControlledFrame(std::string controlled_frame);

    void setq2dotMax(Eigen::VectorXd q2dot_max);

    void setq3dotMax(Eigen::VectorXd q3dot_max);

    void setRegularizationWeight(double regularisation_weight);

    void setRegularizationConfiguration(Eigen::VectorXd  q_mean);

    void setControlPeriod(double  control_period);

    void setRegularizationGains(Eigen::VectorXd regularisation_gains);

    pinocchio::Model getRobotModel();
    std::string getControlledFrame();

    /**
    * \fn bool load_robot 
    * \brief Loads Panda robot
    * \param ros::NodeHandle& node_handle a ros node handle
    * \return true if the robot is correctly loaded
    */
    bool load_robot_model(std::string robot_description, std::vector<std::string> joint_names);

    void getQPLimits(Eigen::VectorXd& q3dot_lim, Eigen::VectorXd& q2dot_lim, Eigen::VectorXd& qdot_lim, Eigen::VectorXd& q_lim_lower, Eigen::VectorXd& q_lim_upper  );
    void getQPConstraints(Eigen::VectorXd& q3dot_constraint, Eigen::VectorXd& q2dot_constraint, Eigen::VectorXd& qdot_constraint, Eigen::VectorXd& q_constraint);

  private:
    // Publishers
    pinocchio::Model model;
    pinocchio::Data data;

    pinocchio::Motion xd;
    Eigen::VectorXd nonLinearTerms; /*!< @brief Matrix representing M^{-1} ( g(q) + c(q,qd) ) */
    pinocchio::Data::Matrix6x J; /*!< @brief Jacobian matrix */
    Eigen::Matrix<double, 6, 1> Jdot_qdot;
    std::string controlled_frame; /*!< @brief tip link of the KDL chain (usually the end effector*/
    bool control_frame_set = false;
    double control_period;
    double regularisation_weight; /*!< @brief Regularisation weight */
    Eigen::VectorXd kp_reg; /*!< @brief Proportional gain for the damping in the regularisation term */
    Eigen::VectorXd q_mean; /*!< @brief Mean joint position of the robot (for the regularization task*/

    QPSolver qp_solver;
    QPSolver::qpProblem qp;
    int number_of_constraints; /*!< @brief Number of constraints of the QP problem*/
    int number_of_variables; /*!< @brief Number of optimization variables of the QP problem*/
    Eigen::Matrix<double, 6, Eigen::Dynamic> a; /*!< @brief Matrix to ease comprehension */
    Eigen::Matrix<double, 6, 1> b; /*!< @brief Matrix to ease comprehension */
    Eigen::VectorXd q2dot_max;
    Eigen::VectorXd q3dot_max;
    Eigen::VectorXd dtorque_max;
    Eigen::VectorXd q3dot_constraint;
    Eigen::VectorXd q2dot_constraint;
    Eigen::VectorXd qdot_constraint;
    Eigen::VectorXd q_constraint;
    Eigen::VectorXd qp_solution;

};
}  // namespace QPController

